#!/usr/bin/env perl

use strict;
use List::Util qw(shuffle);

my $string = " :) " unless $ARGV[0];
$string = " (: " unless not $ARGV[0];

my @prehash = shuffle(1..1000);
my %dontprint;

foreach (1..100) {
	$dontprint{$prehash[$_]} = 1;
}

foreach (1..1000) {
	unless (exists $dontprint{$_}) {print $string};
	unless (not exists $dontprint{$_}) {print "    "};
}

print;
